const fs = require("fs");
const mariadb = require('mariadb');
// read database configuration file | Read Synchrously
let db_config = '';
try {
    db_config = fs.readFileSync('./config/database.json')
}
catch (err) {
    console.log("error, cannot read config/database.json file");
    process.exit();
}

module.exports = mariadb.createPool(JSON.parse(db_config));

