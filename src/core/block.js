module.exports = class Block {
    /**
     * 
     * @param {string} index 
     * @param {string} prevHash 
     * @param {string} hash 
     * @param {Transactions[]} transactions 
     * @param {int} proof 
     */
    constructor (index , prevHash , hash, transactions, proof) {
        this.index = index;
        this.prevHash = prevHash;
        this.hash = hash;
        this.transactions = transactions; // array of type Transaction
        this.proof = proof;
    }

    /* Convert the block to string( for hashin ), I use string instead of json, because  
    if I want to use another programming language like c++ or python, json output is not
    standard (exactlly same)
    */
    toStr() 
    {
        let str  = "index=" + this.index;
        str+= ", hash=" + this.hash;
        str+= ", prevHash=" + this.prevHash;
        str+= ", proof=" + this.proof;
        str+= ", transactions=";
        this.transactions.forEach ( tx => {
            str += '{' + tx.toStr() + '},'
        });
            
        return str;
    }
}