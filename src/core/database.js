const db = require('../utils/database');

module.exports = class DataBase {

    constructor(){};

    /**
     * check if the tables is created, in not create all needed tables
     */
    createTables() {
        console.log("Createing tables ....");
        db.getConnection().then (conn => {
            console.log("connected to database succfully ....");
            conn.query(`SELECT table_name FROM information_schema.tables
            WHERE table_schema = 'kcoins'
            AND table_name = 'blockchain';`).then(rows => {
                if (rows.length == 0) {
                    console.log("Database error, cannot find blockchain table");
                    process.exit();
                }
                else {
                    console.log("Table exist");
                }
            }).catch(err => {
                console.log(err);
            });
           
        }).catch(err => {
            console.log(err);
            console.log("Cannot open database connection!");
            process.exit();
        })
    }
}

