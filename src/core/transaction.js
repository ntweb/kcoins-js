module.exports = class Transaction {

    /**
     * 
     * @param {string} id 
     * @param {string} sender 
     * @param {string} recipient 
     * @param {number} amount 
     */
    constructor(id, sender,recipient, amount) {
        this.id = id; // :string 
        this.sender = sender; //:string
        this.recipient = recipient; //:string
        this.amount = amount; //: number ,,, in JavaScript number = [float, double, int]
    }



     /* Convert the transaction to string( for hashin ), I use string instead of json, because  
        if I want to use another programming language like c++ or python, json output is not
        standard (exactlly same)
    */
   toStr()
   {
        let str  = "amount=" + this.amount;
        str+= ", sender=" + this.sender;
        str+= ", recipient=" + this.recipient;
        str+= ", id=" + this.id;
        return str;
   }
}