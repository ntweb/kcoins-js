const Transaction = require('./transaction');
const Block = require('./block');
const sha256 = require('js-sha256');
const uuid = require('uuid/v1');

module.exports = class Blockchain {

    constructor() {

        /** @type {Block[]} */
        this.chain = [];

        /** @type {Transaction[]} */
        this.bendingTransactions = []; 
        
        this.bendingTransactions.push({'a': 1});
        // add genesis block, Hint: ( first block in the blockchain called genesis )
        this.createNewBlock(100,"0","0");
        
    }




     /**
      * Mine new block
      * @param {number} proof 
      * @param {string} prevHash 
      * @param {string} hash 
      */
     createNewBlock(proof, prevHash, hash) {
        const block = new Block(this.chain.length+1, prevHash, hash, this.bendingTransactions, proof);
        
        // empty current open transaction ofter mining 
        this.bendingTransactions = [];

        // add currently mined block to our chain
        this.chain.push(block);
    }

    
    
    /**
     * add new transaction to  bendingTransaction
     * @param {Transaction} tx 
     * @returns {Transaction}
     */
    createNewTransaction(tx) {
        tx.id = uuid().split('-').join('');
        this.bendingTransactions.push(tx);
        return tx;
    }

    /**
     * get last mined block
     * @returns {Block}
     */
    getLastBlock() {
        return this.chain[this.chain.length-1];
    }

    /**
     * get current block (not mined yet)
     * @returns {Block}
     */
   getCurrentData() {
        const block = new Block(this.chain.length+1, this.getLastBlock().hash, '',this.bendingTransactions, 0);
        return block;
   }
   /**
    * get whole blockchain
    * @returns {Block[]}
    */
   getChain() {
       return this.chain;
   }


   /**
    * hashing the given block
    * @param {string} prevHash 
    * @param {Block} block 
    * @param {number} proof 
    * @returns {string}
    */
   hashBlock(prevHash, block, proof ) {
       const dataAsString = prevHash + proof.toString() + block.toStr()
       const hash = sha256(dataAsString);
       return hash;
   }

   /**
    * POW, finding the right nonce number to hash the block
    * @param {string} prevHash 
    * @param {Block} block 
    * @return {proof}
    */
   profOfWork(prevHash , block) {
       let proof = 0;
       let hash = this.hashBlock(prevHash, block, proof);
       while (hash.substring(0,5) !== '00000') {
           proof++;
           hash = this.hashBlock(prevHash, block, proof); 
       }
       return proof;
   }
}

